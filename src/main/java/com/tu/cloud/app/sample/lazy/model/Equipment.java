package com.tu.cloud.app.sample.lazy.model;

import java.math.BigDecimal;

public class Equipment {
    
    private String name;
    private BigDecimal price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Equipment(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

}

package com.tu.cloud.app.sample.lazy.resource;

import java.util.Optional;

import com.tu.cloud.app.sample.lazy.model.Spaceship;
import com.tu.cloud.app.sample.lazy.repository.SpaceshipRepository;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/spaceship")
public class SpaceshipResource {

    // Redis DB -- check DemoApplicationTests

    private SpaceshipRepository spaceshipRepository;

    public SpaceshipResource(SpaceshipRepository spaceshipRepository) {
        this.spaceshipRepository = spaceshipRepository;
    }

    @GetMapping("/get/{id}")
    public Optional<Spaceship> get(@PathVariable("id") final String id) {
        return spaceshipRepository.findById(id);
    }

    @GetMapping("/all")
    public Iterable<Spaceship> all() {
        return spaceshipRepository.findAll();
    }
}
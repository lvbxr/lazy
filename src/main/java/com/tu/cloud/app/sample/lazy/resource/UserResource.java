
package com.tu.cloud.app.sample.lazy.resource;

import java.util.List;

import com.tu.cloud.app.sample.lazy.model.User;
import com.tu.cloud.app.sample.lazy.repository.UserRepository;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/user")
public class UserResource {

    //Mongo DB

    private UserRepository userRepository;

    public UserResource(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/get/{id}")
    public User get(@PathVariable("id") final String id) {
        return userRepository.findById(id);
    }

    @GetMapping("/add/{id}/{name}")
    public User add(@PathVariable("id") final String id, @PathVariable("name") final String name) {
        userRepository.save(new User(id, name, 20000L));
        return get(id);
    }

    @GetMapping("/update/{id}/{name}")
    public User update(@PathVariable("id") final String id, @PathVariable("name") final String name) {
        userRepository.save(new User(id, name, 1000L));
        return get(id);
    }

    @GetMapping("/delete/{id}")
    public List<User> delete(@PathVariable("id") final String id) {
        User user = get(id);
        userRepository.delete(user);
        return all();
    }

    @GetMapping("/all")
    public List<User> all() {
        return userRepository.findAll();
    }
}
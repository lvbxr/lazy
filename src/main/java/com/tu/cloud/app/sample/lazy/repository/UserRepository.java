package com.tu.cloud.app.sample.lazy.repository;

import com.tu.cloud.app.sample.lazy.model.User;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, Integer> {

	User findById(String id);
}
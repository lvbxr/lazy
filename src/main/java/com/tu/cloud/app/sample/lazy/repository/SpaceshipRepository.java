package com.tu.cloud.app.sample.lazy.repository;

import com.tu.cloud.app.sample.lazy.model.Spaceship;

import org.springframework.data.repository.CrudRepository;

public interface SpaceshipRepository extends CrudRepository<Spaceship, String> {

}
package com.tu.cloud.app.sample.lazy.model;


import org.springframework.data.redis.core.RedisHash;

import java.math.BigDecimal;
import java.util.List;

@RedisHash("CoolSpaceship")
public class Spaceship {
    private String id;
    private String model;
    private BigDecimal weight;
    private List<Equipment> equipment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public List<Equipment> getEquipment() {
        return equipment;
    }

    public void setEquipment(List<Equipment> equipment) {
        this.equipment = equipment;
    }

    public Spaceship(String id, String model, BigDecimal weight, List<Equipment> equipment) {
        this.id = id;
        this.model = model;
        this.weight = weight;
        this.equipment = equipment;
    }

    public Spaceship() {
    }

    
}
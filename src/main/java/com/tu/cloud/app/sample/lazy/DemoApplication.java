package com.tu.cloud.app.sample.lazy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	// @Bean
	// JedisConnectionFactory jedisConnectionFactory() {
	// 	return new JedisConnectionFactory();
	// }

	// @Bean
	// RedisTemplate<String, User> redisTemplate() {
	// 	RedisTemplate<String, User> redisTemplate = new RedisTemplate<>();
	// 	redisTemplate.setConnectionFactory(jedisConnectionFactory());
	// 	return redisTemplate;
 	// }
}

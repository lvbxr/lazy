**sample from**  
https://github.com/ekim197711/springboot-redis/  
#!/usr/bin/env bash  
docker rm -f myredis  
docker run --name myredis -p 6379:6379 -d redis:latest  
docker exec -it myredis redis-cli  




How to use local docker images with Minikube?

https://stackoverflow.com/questions/42564058/how-to-use-local-docker-images-with-minikube


### Start minikube ### 
minikube start

### Set docker env ### 
eval $(minikube docker-env)

### Build image ### 
docker build -t foo:0.0.1 .

###  Run in minikube ### 
kubectl run hello-foo --image=foo:0.0.1 --image-pull-policy=Never

###  Check that it's running
kubectl get pods



### more ###



docker run -d -p 5000:5000 --restart=always --name registry registry:2

kubectl create -f kube.yml

minikube service [image name] 
e.g. minikube service spring-boot-demo



```yaml

#https://stackoverflow.com/questions/33887194/how-to-set-multiple-commands-in-one-yaml-file-with-kubernetes

somme commands examples
apiVersion: v1
kind: Pod
metadata:
  name: hello-world
spec:  # specification of the pod’s contents
  restartPolicy: Never
  containers:
  - name: hello
    image: "ubuntu:14.04"
    env:
    - name: MESSAGE
      value: "hello world"
    command: ["/bin/sh","-c"]
    args: ["/bin/echo \"${MESSAGE}\""]

some multiple commands samples

containers:
  - name: mysqldump
    image: mysql
    command: ["/bin/sh", "-c"]
    args:
      - echo starting;
        ls -la /backups;
        mysqldump --host=... -r /backups/file.sql db_name;
        ls -la /backups;
        echo done;
    volumeMounts:
      - ...

```